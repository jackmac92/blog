#!/usr/bin/env bash
set -euo pipefail

cd ~/Projects/career/blog/hugo

# build hugo markdown from org
e -e '(jm/blog-hugo-export-all)'
# build html from markdown
hugo

rclone sync public s3personal:www.jackm.works
