+++
title = "How I blog"
author = ["Jack McCown"]
date = 2022-06-07
draft = false
+++

blogging about your blog's stack seems like some sort of right of passage, so here goes nothing.

I write my posts in org-mode, use ox-hugo to export org documents as markdown for hugo, and use hugo to build the site.

My **favorite** thing so far is using [org-transclusion](https://nobiot.github.io/org-transclusion) for its abililty to reference source code files and incorporate them in my posts.

For example, in my [previous post on fzf]({{< relref "fzf-select-by-json-key" >}}) I use org-transclusion to reference the actual source file of the script I use. So any changes to the source will be propagated to my blog the next time I publish. And I like that

Ultimately my blog is hosted on s3
