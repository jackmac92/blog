+++
title = "Running goss tests with sequencing"
author = ["Jack McCown"]
date = 2022-06-07
tags = ["shell", "goss"]
draft = false
+++

goss does not include the ability to run tests in a defined sequence
<https://github.com/aelsabbahy/goss/issues/485>

But when e.g. NetworkManager fails for some reason after startup, seeing 200+ failing tests is not a good way to narrow down the cause of the issue

For that we need to sequence the tests goss runs

We can do that using a directory tree of goss tests, fd and xargs

```shell
# s goss run-in-order
TARGET_DIR=${1:?"Missing arg; must be the top level of the goss tests dir"}

# DFS, recursively call self
fd -t d --max-depth=1 . $TARGET_DIR | sort --numeric-sort -r | xargs -I_ bash -c "s goss run-in-order _ || exit 255"
fd -t f -t l --max-depth=1 . $TARGET_DIR | sort --numeric-sort -r | xargs -I_ bash -c '>&2 echo -en "\r\033[Ktesting $(basename _)"; chronic goss -g _ validate --format=json --format-options=verbose || exit 255'
```
