+++
title = "Using goss to fix problems it identifies"
author = ["Jack McCown"]
date = 2022-06-07
tags = ["shell", "goss"]
draft = false
+++

[goss](https://goss.rocks) allows you to attach arbitrary metadata to test specifications

```yaml
dns:
  google.com:
    resolvable: true
    timeout: 500
    meta:
      info: "Basic dns resolution"
```

goss will include this metadata when `--format-options=verbose --format=json`

you don't necessarily need custom annotations to fix goss test errors

```shell
jq -r '.results
    | map(select(.successful | not))
    | map(select(.meta!=null))
    | map(.meta)
    | map(select(.fix!=null))
    | .[]
    | if (.fix | type)=="array" then .fix else [.fix] end
    | .[]' <<<"$testresults"
# autofix missing pkgs
jq -r '.results | map(select((.successful | not) and ."resource-type" == "Package" and .property == "installed"))[]."resource-id"' <<<"$testresults" | rg -v '^\s*$' | xargs --no-run-if-empty -n 1 printf "s util syspkg-install %s\n"
# autofix groups
jq -r '.results
| map(select((.successful | not) and ."resource-type" == "User" and .property == "groups"))[]
| "\(.expected | first | fromjson | join(",")) \(."resource-id")"' <<<"$testresults" | xargs --no-run-if-empty -n 1 printf "sudo usermod --append --groups %s\n"
# autofix services
jq -r '.results | map(select((.successful | not) and ."resource-type" == "Service" and .property == "running"))[]."resource-id"' <<<"$testresults" | xargs --no-run-if-empty -n 1 printf "sudo systemctl restart %s.service\n"
```
